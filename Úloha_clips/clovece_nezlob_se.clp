(defglobal ?*TAKE* =      100) 
(defglobal ?*MOVE* =      100) 
(defglobal ?*PLACE_NEW* = 100) 
(defglobal ?*GO_HOME* =   120) 


(deftemplate FIGURE "One figure on the board"
    (slot POSITION)
    (slot NUM)
    (slot PLAYER)
    (slot TAKE (default 0))
    (slot MOVE (default ?*MOVE*))
    (slot PLACE_NEW (default ?*PLACE_NEW*))
    (slot GO_HOME (default ?*GO_HOME*))
    (slot APPLIED_RULES (default ""))
)

(deftemplate ENEMY_FIGURE "One figure on the board"
    (slot POSITION)
    (slot NUM)
    (slot PLAYER)
)


(deffunction figures_to_facts (?player $?list)
    (bind ?index 1)
    (if (= ?player 1)
    then
        (foreach ?position $?list
            (assert 
                (FIGURE
                    (POSITION ?position)
                    (PLAYER ?player)
                    (NUM ?index)
                    (TAKE 0)
                    (MOVE 0)
                    (PLACE_NEW 0)
                    (GO_HOME 0)
                )
            )
            (bind ?index (+ ?index 1))
        )
    else
        (foreach ?position $?list
            (assert 
                (ENEMY_FIGURE
                    (POSITION ?position)
                    (PLAYER ?player)
                    (NUM ?index)
                )
            )
            (bind ?index (+ ?index 1))
        )
    )
)

 ; START_______________________________________________________________________________
(defrule start "Starts the program and prints instructions to the user"
	?init <- (initial-fact)
		=>
    (retract ?init)
    (printout t crlf crlf crlf)
    (printout t "Vítejte v programu AlphaAngry," crlf)
    (printout t crlf )
    (printout t "díky našemu sofistikovanému algoritmu z Vás uděláme mistra hry Člověče, nezlob se." crlf)
    (printout t "Nejdříve se Vás zeptáme na pár otázek." crlf)

    (printout t crlf crlf crlf)

    (printout t "==============================================================================" crlf)
    (printout t "                             ---VAROVÁNÍ---" crlf) 
    (printout t "S VELKOU MOCÍ PŘICHÁZÍ VELKÁ ZODPOVĚDNOST, POUŽÍVEJTE AlphaAngry S ROZVAHOU!!!" crlf)
    (printout t "==============================================================================" crlf)
    
    (printout t crlf crlf crlf)
    (printout t "Stiskněte \"Enter\" pro start programu." crlf)

    (bind ?answer (readline))
    (assert (values-init))
)
 ; /START_______________________________________________________________________________


 ; QUESTIONINIG_______________________________________________________________________________
(defrule initialization "Here we initialize values for the game"
    ?val-init <- (values-init)
        =>
    (retract ?val-init)

    (printout t crlf "Jaké číslo padlo na kostce?" crlf)
    (bind ?dice (read))

    (printout t crlf "                            ZADÁVÁNÍ POZIC FIGUREK" crlf)
    (printout t crlf crlf "=================================================================================================" crlf)
    (printout t crlf "Pozice zadávejte v následujícím formátu:" crlf)
    (printout t crlf "0 - figurka je na startu" crlf)
    (printout t crlf "1-40 - pozice figurky relativně k Vašemu nasazovacímu poli" crlf)
    (printout t crlf "41-44 pozice figurky v domečku ('a' -> 41, ..., 'd' -> 44)" crlf)
    (printout t crlf "příklad: 0 0 20 43 -> 2 figurky na startu, jedna na 20. poli a jedna v domečku na 3. pozici" crlf)
    (printout t "=================================================================================================" crlf crlf crlf)


    (printout t crlf "Zadejte pozice Vašich figurek" crlf)
    (figures_to_facts 1 (create$ (explode$ (readline))))

    (printout t crlf "Zadejte pozice figurek hráče po Vaší levici" crlf)
    (figures_to_facts 2 (create$ (explode$ (readline))))

    (printout t crlf "Zadejte pozice figurek hráče naproti Vám" crlf)
    (figures_to_facts 3 (create$ (explode$ (readline))))

    (printout t crlf "Zadejte pozice figurek hráče po Vaší pravici" crlf)
    (figures_to_facts 4 (create$ (explode$ (readline))))

    (assert (dice ?dice))

)
; /QUESTIONINIG_______________________________________________________________________________

; FUNCTIONS_______________________________________________________________________________
(deffunction rel_figure_pos (?pos ?player) "Gets figure position relative to their start or false if figure is in home or hasn't even started"
    (if (or (< ?pos 1) (> ?pos 40) )
        then 
            (return -1)
        else 
            (bind ?player_spawn_position_koeficient ( - 40 (* 10 (- ?player 1))))
            (return (+ (mod (+ (- ?pos 1) ?player_spawn_position_koeficient) 40) 1)) ;- relative enemy position
    )
)

(deffunction figure_pos (?rel_pos ?player) "Gets figure position relative to player 1"
    (if (or (< ?rel_pos 1) (> ?rel_pos 40))
        then
            (return -1)
        else
            (bind ?player_spawn_position_koeficient (* 10 (- ?player 1)))
            (return (+ (mod (- 1 (+ ?rel_pos ?player_spawn_position_koeficient)) 40) 1))
    )
)

(deffunction is_home (?fig_pos) ""
    (return (and (> ?fig_pos 40) (< ?fig_pos 45)))
)

(deffunction is_on_start (?fig_pos) ""
    (return (= ?fig_pos 0))
)

(deffunction is_on_board (?fig_pos) ""
    (return (and (not (is_home ?fig_pos)) (not (is_on_start ?fig_pos))))
)

; positions are relevant to player 1 spawn
(deffunction fig_take_distance (?player ?player_fig_pos ?other_fig_pos) "Get take distance relative to player figure (<= 0 means player figure never takes other figure)"
    ; check if either pos is not on board -> player_fig never takes other_fig
    (if (or
        (not (is_on_board ?player_fig_pos))
        (not (is_on_board ?other_fig_pos)))
        then
            (return -41)
    )

    (if (!= ?player 1)
        then
            (bind ?player_fig_pos   (rel_figure_pos ?player_fig_pos ?player))
            (bind ?other_fig_pos    (rel_figure_pos ?other_fig_pos ?player))
    )

    (return (- ?other_fig_pos ?player_fig_pos))
)

(deffunction fig_distance (?player_fig_pos ?enemy_fig_pos) "Get distance between 2 figures - 1st figure is you, 2nd figure is enemy."
    (if (> ?player_fig_pos ?enemy_fig_pos)
    then
        (return (- ?player_fig_pos ?enemy_fig_pos) )
    else
        (return (- 40 (abs (- ?player_fig_pos ?enemy_fig_pos))) )
    )
)

; /FUNCTIONS_______________________________________________________________________________


; GO_HOME_______________________________________________________________________________
(defrule home0 "Pokud figurka vstoupí do domečku, vyhráli jsme?"
    ?fig_fact <- (FIGURE (POSITION ?position_me) (PLAYER 1) (NUM ?index_me) (TAKE ?take) (MOVE ?move) (PLACE_NEW ?place_new) (GO_HOME ?go_home) (APPLIED_RULES ?applied_rules))
    ?fig_fact2 <- (FIGURE (POSITION ?position_me2))

    (test (not (str-index "home0" ?applied_rules)))                     ; checks if this rule has been aplied
    (dice ?dice)
    (test (> (+ ?position_me ?dice) 40))
    (test (< (+ ?position_me ?dice) 45))
    (test (!= (+ ?position_me ?dice) ?position_me2))
        =>
    ; (printout t "home1" crlf)
    (retract ?fig_fact)
    (assert (FIGURE (POSITION ?position_me) (PLAYER 1) (NUM ?index_me) (TAKE ?take) (MOVE ?move) (PLACE_NEW ?place_new) (GO_HOME ?*GO_HOME*) (APPLIED_RULES (str-cat ?applied_rules "home0 ")))) 
)


(defrule home1 "Pokud figurka vstoupí do domečku, vyhráli jsme?"
    ?fig_fact <- (FIGURE (POSITION ?position_me) (PLAYER 1) (NUM ?index_me) (TAKE ?take) (MOVE ?move) (PLACE_NEW ?place_new) (GO_HOME ?go_home) (APPLIED_RULES ?applied_rules))
    ?fig_fact_field <- (FIGURE (POSITION ?position))
    (test (= (length$ (find-all-facts ((?f FIGURE)) (and (> ?f:POSITION 40) (!= ?f:NUM ?index_me)))) 3))
    (test (not (str-index "home1" ?applied_rules)))                     ; checks if this rule has been aplied
    (dice ?dice)
    (test (> (+ ?position_me ?dice) 40))
    (test (< (+ ?position_me ?dice) 45))
    (test (!= (+ ?position_me ?dice) ?position))
        =>
    ; (printout t "home1" crlf)
    (retract ?fig_fact)
    (assert (FIGURE (POSITION ?position_me) (PLAYER 1) (NUM ?index_me) (TAKE ?take) (MOVE ?move) (PLACE_NEW ?place_new) (GO_HOME (* ?go_home 1000)) (APPLIED_RULES (str-cat ?applied_rules "home1 ")))) 
)

(defrule home2 "figurka je 6 nebo méně polí před nepřítelem tak, že mě nepřítel může vyhodit"
    ?fig_fact <- (FIGURE (POSITION ?position_me) (PLAYER 1) (NUM ?index_me) (TAKE ?take) (MOVE ?move) (PLACE_NEW ?place_new) (GO_HOME ?go_home) (APPLIED_RULES ?applied_rules))
    ?fig_fact_enemy <- (ENEMY_FIGURE (POSITION ?position_enemy) (PLAYER ?player))
    ?fig_fact_field <- (FIGURE (POSITION ?position) (NUM ?index) (PLAYER 1))
    (test (not (str-index "home2" ?applied_rules)))                     ; checks if this rule has been aplied
    (dice ?dice)
    (test (< ?position_me 41))
    (test (> (+ ?position_me ?dice) 40))
    (test (< (+ ?position_me ?dice) 45))
    (test (!= (rel_figure_pos ?position_enemy ?player) -1))             ; checks if enemy is in the field (not home or not even started)
    (test (> ?position_me ?position_enemy))
    (test (<= (- ?position_me ?position_enemy) 6))
    (test (!= ?index_me ?index))
    (test (!= (+ ?position_me ?dice) ?position))
        =>
    (printout t "home2" crlf)
    (retract ?fig_fact)
    (assert (FIGURE (POSITION ?position_me) (PLAYER 1) (NUM ?index_me) (TAKE ?take) (MOVE ?move) (PLACE_NEW ?place_new) (GO_HOME (* ?go_home 1.1)) (APPLIED_RULES (str-cat ?applied_rules "home2 ")))) 
)

  ; Pokud fig. Vstopí do domečku, vyhráli jsme? => x1000 
  ; Pokud je figurka 6 nebo méně polí před nepřítelem => x1.1 // nutné určit správně prioritu 

; /GO_HOME_______________________________________________________________________________


; PLACE_NEW_______________________________________________________________________________
(defrule place1 "Checks if number 6 is on the dice"
    ?fig_fact <- (FIGURE (POSITION 0) (PLAYER 1) (NUM ?index) (APPLIED_RULES ?applied_rules))
    (test (not (str-index "place1" ?applied_rules))) ; checks if this rule has been aplied
    (dice ?dice)
    (test (= ?dice 6))
        =>
    (retract ?fig_fact)
    ; (printout t "place1" crlf)
    (assert (FIGURE (POSITION 0) (PLAYER 1) (NUM ?index) (TAKE 0) (MOVE 0) (PLACE_NEW ?*PLACE_NEW*) (GO_HOME 0) (APPLIED_RULES (str-cat ?applied_rules "place1 "))))
)

(defrule place2 "nepřítel je 6 a méně polí za naším startem"
    ?fig_fact <- (FIGURE (POSITION 0) (PLAYER 1) (NUM ?index) (TAKE ?take) (MOVE ?move) (PLACE_NEW ?place_new) (GO_HOME ?go_home) (APPLIED_RULES ?applied_rules))
    ?fig_fact_enemy <- (ENEMY_FIGURE (POSITION ?position_enemy))
    (test (not (str-index "place2" ?applied_rules))) ; checks if this rule has been aplied
    (test (> ?position_enemy 1))
    (test (< ?position_enemy 7))
        =>
    ; (printout t "place2" crlf)
    (retract ?fig_fact)
    (assert (FIGURE (POSITION 0) (PLAYER 1) (NUM ?index) (TAKE ?take) (MOVE ?move) (PLACE_NEW (* ?place_new 1.15)) (GO_HOME ?go_home) (APPLIED_RULES (str-cat ?applied_rules "place2 "))))
)

(defrule place3 "mame figurku 10 a mene pozic za startem"
    ?fig_fact <- (FIGURE (POSITION 0) (PLAYER 1) (NUM ?index) (TAKE ?take) (MOVE ?move) (PLACE_NEW ?place_new) (GO_HOME ?go_home) (APPLIED_RULES ?applied_rules))
    ?fig_fact_field <- (FIGURE (POSITION ?position) (PLAYER 1))
    (test (not (str-index "place3" ?applied_rules))) ; checks if this rule has been aplied
    (test (> ?position 0))
    (test (<= ?position 10))
        =>
    ; (printout t "place3" crlf)
    (retract ?fig_fact)
    (assert (FIGURE (POSITION 0) (PLAYER 1) (NUM ?index) (TAKE ?take) (MOVE ?move) (PLACE_NEW (* ?place_new 0.90)) (GO_HOME ?go_home) (APPLIED_RULES (str-cat ?applied_rules "place3 "))))
)

(defrule place4 "nepřátelská figurka se nachází na našem startu"
    ?fig_fact <- (FIGURE (POSITION 0) (PLAYER 1) (NUM ?index) (TAKE ?take) (MOVE ?move) (PLACE_NEW ?place_new) (GO_HOME ?go_home) (APPLIED_RULES ?applied_rules))
    ?fig_fact_enemy <- (ENEMY_FIGURE (POSITION 1))
    (test (not (str-index "place4" ?applied_rules))) ; checks if this rule has been aplied
        =>
    ; (printout t "place4" crlf)
    (retract ?fig_fact)
    (assert (FIGURE (POSITION 0) (PLAYER 1) (NUM ?index) (TAKE ?take) (MOVE ?move) (PLACE_NEW (* ?place_new 1.5)) (GO_HOME ?go_home) (APPLIED_RULES (str-cat ?applied_rules "place4 "))))
)

(defrule place5 "nepřítel je 6 a méně polí před naším startem"
    ?fig_fact <- (FIGURE (POSITION 0) (PLAYER 1) (NUM ?index) (TAKE ?take) (MOVE ?move) (PLACE_NEW ?place_new) (GO_HOME ?go_home) (APPLIED_RULES ?applied_rules))
    ?fig_fact_enemy <- (ENEMY_FIGURE (POSITION ?position_enemy))
    (test (not (str-index "place5" ?applied_rules))) ; checks if this rule has been aplied
    (test (> ?position_enemy 35))
    (test (< ?position_enemy 41))
        =>
    ; (printout t "place2" crlf)
    (retract ?fig_fact)
    (assert (FIGURE (POSITION 0) (PLAYER 1) (NUM ?index) (TAKE ?take) (MOVE ?move) (PLACE_NEW (* ?place_new 0.5)) (GO_HOME ?go_home) (APPLIED_RULES (str-cat ?applied_rules "place5 "))))
)


  ; nepřítel je 6 a méně polí před naším startem => x1.15 
  ; naše poslední figurka je aspoň 10 polí za startem => x1.05 - POUPRAVENO
  ; nepřátelská figurka se nachází na našem startu => x1.3 - ZKONTROLOVAT

; /PLACE_NEW_______________________________________________________________________________

; /TAKE____________________________________________________________________________________
(defrule take0 "Na hozené číslo lze nepřátelskou figurku vyhodit."
    ?fig_fact <- (FIGURE (POSITION ?position_me) (PLAYER 1) (NUM ?index) (GO_HOME ?go_home) (APPLIED_RULES ?applied_rules))
    ?fig_fact_ennemy <- (ENEMY_FIGURE (POSITION ?position_enemy) (PLAYER ?player_enemy))

    (test (not (str-index "take0" ?applied_rules)))                                 ; checks if this rule has been aplied    
    (dice ?dice)                                                                    ; get the dice
    (test (!= (rel_figure_pos ?position_me 1) -1))                                  ; test if its not figure in base
    (test (!= (rel_figure_pos ?position_enemy ?player_enemy) -1))                         ; test if its not enemy in home
    (test (= (+ ?position_me ?dice) ?position_enemy))
      =>
    (retract ?fig_fact)
    (printout t "take0" crlf)
    (assert (FIGURE (POSITION ?position_me) (PLAYER 1) (NUM ?index) (TAKE ?*TAKE*) (MOVE 0) (PLACE_NEW 0) (GO_HOME ?go_home) (APPLIED_RULES (str-cat ?applied_rules "take0 "))))
)


;   (defrule take1 "Nepřátelská figurka je 10 polí a méně před jejím domečkem" )
(defrule take1 "Nepřátelská figurka je 10 polí a méně před jejím domečkem"
    ?fig_fact <- (FIGURE (POSITION ?position_me) (PLAYER 1) (NUM ?index) (TAKE ?take) (GO_HOME ?go_home) (APPLIED_RULES ?applied_rules))
    ?fig_fact_enemy <- (ENEMY_FIGURE (POSITION ?position_enemy) (PLAYER ?player))
    (test (not (str-index "take1" ?applied_rules)))                                 ; checks if this rule has been aplied
    (dice ?dice)                                                                    ; get the dice
    
    (test (!= (rel_figure_pos ?position_me 1) -1))                                  ; test if its not figure in base
    (test (!= (rel_figure_pos ?position_enemy ?player) -1))                         ; test if its not enemy in home

    (test (= ?position_enemy (+ ?position_me ?dice)))                               ; test if i can step on enemy with dice
    (test (> (rel_figure_pos ?position_enemy ?player) 30))                          ; enemy has less than 10 steps till home
        =>
    (retract ?fig_fact)
    (printout t "take1" crlf)
    (assert (FIGURE (POSITION ?position_me) (PLAYER 1) (NUM ?index) (TAKE (* ?take 1.15)) (MOVE 0) (PLACE_NEW 0) (GO_HOME ?go_home) (APPLIED_RULES (str-cat ?applied_rules "take1 "))))
)

;   (defrule take2 "Nepřátelská figurka je mezi 11 a 20 poli před  jejím domečkem" )
(defrule take2 "Nepřátelská figurka je mezi 11 a 20 poli před jejím domečkem"
    ?fig_fact <- (FIGURE (POSITION ?position_me) (PLAYER 1) (NUM ?index) (TAKE ?take) (GO_HOME ?go_home) (APPLIED_RULES ?applied_rules))
    ?fig_fact_enemy <- (ENEMY_FIGURE (POSITION ?position_enemy) (PLAYER ?player))
    (test (not (str-index "take2" ?applied_rules)))                                 ; checks if this rule has been aplied
    (dice ?dice)                                                                    ; get the dice
    
    (test (!= (rel_figure_pos ?position_me 1) -1))                                  ; test if its not figure in base
    (test (!= (rel_figure_pos ?position_enemy ?player) -1))                         ; test if its not enemy in home
    
    (test (= ?position_enemy (+ ?position_me ?dice)))                               ; test if i can step on enemy with dice
    (test (>= (rel_figure_pos ?position_enemy ?player) 21))                          ; enemy has less than 20 steps till home
    (test (<= (rel_figure_pos ?position_enemy ?player) 30))                         ; 
        =>
    (retract ?fig_fact)
    (printout t "take2" crlf)
    (assert (FIGURE (POSITION ?position_me) (PLAYER 1) (NUM ?index) (TAKE (* ?take 1.10)) (MOVE 0) (PLACE_NEW 0) (GO_HOME ?go_home) (APPLIED_RULES (str-cat ?applied_rules "take2 "))))
)

;   (defrule take3 "Nepřátelská figurka je mezi 21 a 30 poli před jejím domečkem" )
(defrule take3 "Nepřátelská figurka je mezi 21 a 30 poli před jejím domečkem"
    ?fig_fact <- (FIGURE (POSITION ?position_me) (PLAYER 1) (NUM ?index) (TAKE ?take) (GO_HOME ?go_home) (APPLIED_RULES ?applied_rules))
    ?fig_fact_enemy <- (ENEMY_FIGURE (POSITION ?position_enemy) (PLAYER ?player))
    (test (not (str-index "take3" ?applied_rules)))                                 ; checks if this rule has been aplied
    (dice ?dice)                                                                    ; get the dice
    (test (!= (rel_figure_pos ?position_me 1) -1))                                  ; test if its not figure in base
    (test (!= (rel_figure_pos ?position_enemy ?player) -1))                         ; test if its not enemy in home
    (test (= ?position_enemy (+ ?position_me ?dice)))                               ; test if i can step on enemy with dice
    (test (>= (rel_figure_pos ?position_enemy ?player) 11))                ; enemy has less than 30 steps till home
    (test (<= (rel_figure_pos ?position_enemy ?player) 20))                ;
        =>      
    (retract ?fig_fact)
    (printout t "take3" crlf)
    (assert (FIGURE (POSITION ?position_me) (PLAYER 1) (NUM ?index) (TAKE (* ?take 1.05)) (MOVE 0) (PLACE_NEW 0) (GO_HOME ?go_home) (APPLIED_RULES (str-cat ?applied_rules "take3 "))))
)

;   (defrule take4 "Nepřátelská figurka (kterou můžeme vyhodit) je na startu && nepřítel ještě má figurky k nasazení" ) ;TODO
(defrule take4 "Nepřátelská figurka (kterou můžeme vyhodit) je na startu && nepřítel ještě má figurky k nasazení"
    ?fig_fact <- (FIGURE (POSITION ?position_me) (PLAYER 1) (NUM ?index) (TAKE ?take) (GO_HOME ?go_home) (APPLIED_RULES ?applied_rules))
    ?fig_fact_enemy <- (ENEMY_FIGURE (POSITION ?position_enemy) (PLAYER ?player_enemy))
    ?fig_fact_enemy_start <- (ENEMY_FIGURE (POSITION 0) (PLAYER ?player_enemy_start))
    (test (not (str-index "take4" ?applied_rules)))                                 ; checks if this rule has been aplied
    (dice ?dice)                                                                    ; get the dice

    (test (!= (rel_figure_pos ?position_me 1) -1))                                  ; test if its not figure in base
    (test (!= (rel_figure_pos ?position_enemy ?player_enemy) -1))                   ; test if its not enemy in home
    (test (= ?position_enemy (+ ?position_me ?dice)))                               ; test if i can step on enemy with dice

    
;    (test (= (rel_figure_pos ?position_enemy ?player_enemy) 1))                    ; test if enemy stands on any starting field !!!!!!! NEMUSI STAT NA SVEM STARTU
    (test (= (+ (* (- ?player_enemy_start 1) 10) 1)?position_enemy))                     ; test if enemy where enemy stands on starting field has any figures to place


        =>
    (retract ?fig_fact)
    (printout t "take4" crlf)
    (assert (FIGURE (POSITION ?position_me) (PLAYER 1) (NUM ?index) (TAKE (* ?take 0.95)) (MOVE 0) (PLACE_NEW 0) (GO_HOME ?go_home) (APPLIED_RULES (str-cat ?applied_rules "take4 "))))
)

; /TAKE____________________________________________________________________________________

; MOVE_______________________________________________________________________________
(defrule move0
    ?fig_fact <- (FIGURE (POSITION ?position_me) (PLAYER 1) (NUM ?index)(APPLIED_RULES ?applied_rules))
    (dice ?dice)
    (test (not (str-index "move0" ?applied_rules)))
    (test (!= (rel_figure_pos ?position_me 1) -1))                                                       ; checks if this rule has been aplied
    (test (!= (rel_figure_pos (+ ?position_me ?dice)1) -1))                                                       ; checks if this rule has been aplied
    (test (= 0 (length$ (find-all-facts ((?f ENEMY_FIGURE)) (= ?f:POSITION (+ ?position_me ?dice))))))
        =>
    (retract ?fig_fact)
    (assert (FIGURE (POSITION ?position_me) (PLAYER 1) (NUM ?index) (TAKE 0) (MOVE ?*MOVE*) (PLACE_NEW 0) (GO_HOME 0) (APPLIED_RULES (str-cat ?applied_rules "move0 "))))
)

(defrule move1 "Dostane se hrac po tahu pred souperovu figurku..?"
    ?fig_fact <- (FIGURE (POSITION ?position_me) (PLAYER 1) (NUM ?index) (MOVE ?move) (APPLIED_RULES ?applied_rules))
    (test (not (str-index "move1" ?applied_rules)))
    (test (!= ?move 0))
    (ENEMY_FIGURE (POSITION ?position_enemy))
    (dice ?dice)
    (test (> ?position_enemy ?position_me))
    (test (> (+ ?dice ?position_me) ?position_enemy))
        =>
    (retract ?fig_fact)
    (assert (FIGURE (POSITION ?position_me) (PLAYER 1) (NUM ?index) (TAKE 0) (MOVE (* ?move 0.9)) (PLACE_NEW 0) (GO_HOME 0) (APPLIED_RULES (str-cat ?applied_rules "move1 "))))
)

(defrule move2 "Nejvzdalenejsi figurka ve hre"
    ?fig_fact <- (FIGURE (POSITION ?position_me) (PLAYER 1) (NUM ?index) (MOVE ?move) (APPLIED_RULES ?applied_rules))
    (test (not (str-index "move2" ?applied_rules)))
    (test (!= ?move 0))
    (not (FIGURE (POSITION ?pos2&:(> ?pos2 ?position_me))))
    (test (!= (rel_figure_pos ?position_me 1) -1))                                                       ; checks if this rule has been aplied
    =>
    (retract ?fig_fact)
    (assert (FIGURE (POSITION ?position_me) (PLAYER 1) (NUM ?index) (TAKE 0) (MOVE (* ?move 1.05)) (PLACE_NEW 0) (GO_HOME 0) (APPLIED_RULES (str-cat ?applied_rules "move2 "))))
)


; Dostaneme se tahem o n polí na start nepřítele => x0.75
(defrule move3 "Dostaneme se tahem o n polí na start nepřítele"
    ?fig_fact <- (FIGURE (POSITION ?position) (PLAYER 1) (NUM ?index) (MOVE ?move) (APPLIED_RULES ?applied_rules))
    (test (not (str-index "move3" ?applied_rules)))                                 ; checks if this rule has been aplied
    (test (!= ?move 0))
    (dice ?dice)
    ; check if figure is on enemy spawn
    (test (or
        (= (+ ?position ?dice) 11)
        (= (+ ?position ?dice) 21)
        (= (+ ?position ?dice) 31)
    ))
        =>
    (retract ?fig_fact)
    (assert (FIGURE (POSITION ?position) (PLAYER 1) (NUM ?index) (TAKE 0) (MOVE (* ?move 0.75)) (PLACE_NEW 0) (GO_HOME 0) (APPLIED_RULES (str-cat ?applied_rules "move3 "))))
)


; /FIND BEST SOLUTION

(defrule find-max-take
   (declare (salience -100))
   (FIGURE (TAKE ?take1))
   (not (FIGURE (TAKE ?take2&:(> ?take2 ?take1))))
   (not (MAX_TAKE_DONE))
    =>
   (assert (MAX_TAKE ?take1))
   (assert (MAX_TAKE_DONE))
)

(defrule find-max-move
    (declare (salience -100))
    (FIGURE (MOVE ?move1))
    (not (FIGURE (MOVE ?move2&:(> ?move2 ?move1))))
    (not (MAX_MOVE_DONE))
        =>
    (assert (MAX_MOVE ?move1))
    (assert (MAX_MOVE_DONE))

)

(defrule find-max-place
    (declare (salience -100))
    (FIGURE (PLACE_NEW ?place1))
    (not (FIGURE (PLACE_NEW ?place2&:(> ?place2 ?place1))))
    (not (MAX_PLACE_DONE))
        =>
    (assert (MAX_PLACE ?place1))
    (assert (MAX_PLACE_DONE))

)

(defrule find-max-home
    (declare (salience -100))
    (FIGURE (GO_HOME ?home1))
    (not (FIGURE (GO_HOME ?home2&:(> ?home2 ?home1))))
    (not (MAX_HOME_DONE))
        =>
    (assert (MAX_HOME ?home1))
    (assert (MAX_HOME_DONE))
)

(defrule find-best-solution-place
    (declare (salience -120))
    (MAX_TAKE ?take)
    (MAX_MOVE ?move)
    (MAX_PLACE ?place)
    (MAX_HOME ?home)
    (FIGURE (PLACE_NEW ?place) (NUM ?index))
    (test (> ?place ?move))
    (test (> ?place ?take))
    (test (> ?place ?home))
        =>
    (printout t "I have decided the best move for you might be placing new figure:" ?index crlf)
)

(defrule find-best-solution-move
    (declare (salience -120))
    (MAX_TAKE ?take)
    (MAX_MOVE ?move)
    (MAX_PLACE ?place)
    (MAX_HOME ?home)
    (FIGURE (MOVE ?move) (NUM ?index))
    (test (> ?move ?take))
    (test (> ?move ?place))
    (test (> ?move ?home))
        =>
    (printout t "I have decided the best move for you might be moving figure:" ?index crlf)
)

(defrule find-best-solution-take
    (declare (salience -120))
    (MAX_TAKE ?take)
    (MAX_MOVE ?move)
    (MAX_PLACE ?place)
    (MAX_HOME ?home)
    (FIGURE (TAKE ?take) (NUM ?index))
    (test (> ?take ?move))
    (test (> ?take ?place))
    (test (> ?take ?home))
        =>
    (printout t "I have decided the best move for you might be attacking by figure:" ?index crlf)
)

(defrule find-best-solution-home
    (declare (salience -120))
    (MAX_TAKE ?take)
    (MAX_MOVE ?move)
    (MAX_PLACE ?place)
    (MAX_HOME ?home)
    (FIGURE (GO_HOME ?home) (NUM ?index))
    (test (> ?home ?take))
    (test (> ?home ?move))
    (test (> ?home ?place))
        =>
    (printout t "I have decided the best move for you might going home by figure:" ?index crlf)
)
 
(defrule no-solution
    (declare (salience -120))
    (MAX_TAKE ?x1)
    (MAX_MOVE ?x2)
    (MAX_PLACE ?x3)
    (MAX_HOME ?x4)
    (test (= 0 ?x1 ?x2 ?x3 ?x4))
        =>
    (printout t "You can't do anything" crlf)
)
