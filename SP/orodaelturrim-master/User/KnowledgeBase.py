import math 
from typing import List
from OrodaelTurrim.Business.Interface.Player import PlayerTag
from OrodaelTurrim.Business.Proxy import MapProxy, GameObjectProxy, GameUncertaintyProxy
from ExpertSystem.Business.UserFramework import IKnowledgeBase
from ExpertSystem.Structure.RuleBase import Fact
from OrodaelTurrim.Structure.Enums import TerrainType, AttributeType, EffectType, GameRole, GameObjectType
from OrodaelTurrim.Structure.Position import OffsetPosition, CubicPosition, AxialPosition
from User.Fuzzy import FuzzyInputVariable


class KnowledgeBase(IKnowledgeBase):
    """
    Class for defining known facts based on Proxy information. You can transform here any information from
    proxy to better format of Facts. Important is method `create_knowledge_base()`. Return value of this method
    will be passed to `Inference.interfere`. It is recommended to use Fact class but you can use another type.

    |
    |
    | Class provides attributes:

    - **map_proxy [MapProxy]** - Proxy for access to map information
    - **game_object_proxy [GameObjectProxy]** - Proxy for access to all game object information
    - **uncertainty_proxy [UncertaintyProxy]** - Proxy for access to all uncertainty information in game
    - **player [PlayerTag]** - class that serve as instance of user player for identification in proxy methods

    """
    map_proxy: MapProxy
    game_object_proxy: GameObjectProxy
    game_uncertainty_proxy: GameUncertaintyProxy
    player: PlayerTag


    def __init__(self, map_proxy: MapProxy, game_object_proxy: GameObjectProxy,
                 game_uncertainty_proxy: GameUncertaintyProxy, player: PlayerTag):
        """
        You can add some code to __init__ function, but don't change the signature. You cannot initialize
        KnowledgeBase class manually so, it is make no sense to change signature.
        """
        super().__init__(map_proxy, game_object_proxy, game_uncertainty_proxy, player)


    def create_knowledge_base(self) -> List[Fact]:
        """
        Method for create user knowledge base. You can also have other class methods, but entry point must be this
        function. Don't change the signature of the method, you can change return value, but it is not recommended.

        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        !!  TODO: Write implementation of your knowledge base definition HERE   !!
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """

        facts = []

        if not self.map_proxy.player_have_base(self.player):
            facts.append(Fact('player_dont_have_base', lambda: 1.0))
        else:
            facts.append(Fact('player_dont_have_base', lambda: 0.0))


        facts.append(Fact( 'is_money_low', eval_function=self.is_money_low ))
        facts.append(Fact( 'is_money_high', eval_function=self.is_money_high ))


        facts.append(Fact( 'is_base_safe', eval_function=self.is_base_safe ))
        facts.append(Fact( 'is_base_relitively_safe', eval_function=self.is_base_relitively_safe ))
        facts.append(Fact( 'is_base_relitively_not_safe', eval_function=self.is_base_relitively_not_safe ))
        facts.append(Fact( 'is_base_not_safe', eval_function=self.is_base_not_safe ))

        facts.append(Fact( 'is_visibility_low', eval_function=self.is_visibility_low ))
        facts.append(Fact( 'is_visibility_high', eval_function=self.is_visibility_high ))


        return facts

    def fuzzy_money(self) -> FuzzyInputVariable:
        money = FuzzyInputVariable( "Money" )

        money.addSet( "Low", lambda x: ( 1 - ( ( x - 15 ) / 135.0 ) ) )
        money.addSet( "High", lambda x: ( ( x - 15 ) / 135.0 ) )

        money.fuzzify( self.game_object_proxy.get_resources(self.player) )

        return money

    def is_money_low(self):
        return self.fuzzy_money().getDOM( "Low" )

    def is_money_high(self):
        return self.fuzzy_money().getDOM( "High" )


    def fuzzy_safety( self ) -> FuzzyInputVariable:
        safety = FuzzyInputVariable( "Safety" )

        safety.addSet( "Safe", lambda x: ( ( ( x + 150 ) * -0.1 ) + 2 ) )
        safety.addSet( "Relatively Safe", lambda x: ( ( -0.1 * abs( x + 115 ) ) + 2 ) )
        safety.addSet( "Relatively Not Safe", lambda x: ( ( -0.1 * abs( x - 85 ) ) + 2 ) )
        safety.addSet( "Not Safe", lambda x: ( ( ( x + 50 ) * 0.1 ) + 2 ) )


        danger_index = 0.0
        count = 0.0
        for position in self.map_proxy.get_player_visible_tiles():
            if self.map_proxy.is_position_occupied( position ):
                index = 0
                count += 1.0
                for base_position in self.map_proxy.get_bases_positions():

                    health = self.game_object_proxy.get_attribute( position, AttributeType.HIT_POINTS )
                    distance = position.distance_from( base_position )

                    if ( distance - 0.9 ) > 0:
                        index = health / ( math.log( distance - 0.9, 3 ) )

                if self.game_object_proxy.get_role( position ) == GameRole.ATTACKER:
                    danger_index += index
                if self.game_object_proxy.get_role( position ) == GameRole.DEFENDER:
                    if self.game_object_proxy.get_object_type( position ) != GameObjectType.BASE:
                        danger_index -= index

        if count == 0.0:
            safety.fuzzify( 0 )
        else:
            safety.fuzzify( ( danger_index / count ) )

        return safety

    def is_base_safe( self ):
        return self.fuzzy_safety().getDOM( "Safe" )

    def is_base_relitively_safe( self ):
        return self.fuzzy_safety().getDOM( "Relatively Safe" )

    def is_base_relitively_not_safe( self ):
        return self.fuzzy_safety().getDOM( "Relatively Not Safe" )

    def is_base_not_safe( self ):
        return self.fuzzy_safety().getDOM( "Not Safe" )


    def fuzzy_visibility( self ) -> FuzzyInputVariable:
        visibility = FuzzyInputVariable( "Visibility" )

        visibility.addSet( "Low", lambda x: ( 1 - ( ( x - 20 ) / 70.0 ) ) )
        visibility.addSet( "High", lambda x: ( ( x - 20 ) / 70.0 ) )

        visibility.fuzzify( len( self.map_proxy.get_player_visible_tiles() ) )

        return visibility

    def is_visibility_low(self):
        return self.fuzzy_visibility().getDOM( "Low" )

    def is_visibility_high(self):
        return self.fuzzy_visibility().getDOM( "High" )

