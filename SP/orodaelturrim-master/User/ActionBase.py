from OrodaelTurrim.Business.Interface.Player import PlayerTag
from OrodaelTurrim.Business.Proxy import GameControlProxy
from ExpertSystem.Business.UserFramework import IActionBase
from OrodaelTurrim.Business.Logger import Logger
from OrodaelTurrim.Structure.Enums import GameObjectType, TerrainType, GameRole
from OrodaelTurrim.Structure.Filter.AttackFilter import AttackStrongestFilter, AttackMostVulnerableFilter
from OrodaelTurrim.Structure.Filter.Factory import FilterFactory
from OrodaelTurrim.Structure.GameObjects.GameObject import SpawnInformation
from OrodaelTurrim.Structure.Position import OffsetPosition

from User.AttackFilter import DummyAttackFilter, EmptyAttackFilter, CanAttackBaseAttackFilter


class ActionBase(IActionBase):
    """
    You can define here your custom actions. Methods must be public (not starting with __ or _) and must have unique
    names. Methods could have as many arguments as you want. Instance of this class will be available in
    Inference class.

    **This class provides:**

    * self.game_control_proxy [GameControlProxy] for doing actions in game
    * self.player [PlayerTag] instance of your player for identification yourself in proxy

    Usage of ActionBase is described in documentation.


    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!               TODO: Write implementation of your actions HERE                !!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    """
    game_control_proxy: GameControlProxy
    player: PlayerTag


    def build_base(self):

        base_position = {}; 
        dont_want_near = [TerrainType.MOUNTAIN, TerrainType.HILL, TerrainType.FOREST]
        border_tiles = self.map_proxy.get_border_tiles()


        # find suitable tile for my base
        for tolerance in range(6): # start with low tolerace for visibility but over time increase it
            for position in self.map_proxy.get_inner_tiles():

                if self.map_proxy.get_terrain_type(position) == TerrainType.FOREST:

                    if position not in border_tiles:
                        unwanted_count = 0

                        for neighbour in position.get_all_neighbours():

                            if self.map_proxy.get_terrain_type(neighbour) in dont_want_near:
                                unwanted_count += 1

                        if unwanted_count <= tolerance:
                            base_position = position



        empty_filter = FilterFactory().attack_filter(EmptyAttackFilter)
        strongest_filter = FilterFactory().attack_filter(AttackStrongestFilter)

        self.game_control_proxy.spawn_unit(
            SpawnInformation(self.player,
                             GameObjectType.BASE,
                             base_position,
                             [empty_filter, strongest_filter], []))


    def build_scout(self):

        tiles = self.map_proxy.get_player_visible_tiles()
        border_tiles = self.map_proxy.get_border_tiles()

        scout_tile = None

        for position in tiles:
            terrain = self.map_proxy.get_terrain_type(position) == TerrainType.MOUNTAIN
            terrain = terrain or self.map_proxy.get_terrain_type(position) == TerrainType.HILL
            occupied = self.map_proxy.is_position_occupied(position)
            if terrain and not occupied and position not in border_tiles:
                scout_tile = position
                break

        empty_filter = FilterFactory().attack_filter(EmptyAttackFilter)
        attack_vulnerable = FilterFactory().attack_filter( AttackMostVulnerableFilter )

        if scout_tile is not None:

            self.game_control_proxy.spawn_unit(
                SpawnInformation(self.player,
                                GameObjectType.ARCHER,
                                scout_tile,
                                [empty_filter, attack_vulnerable], []))


    def build_archer( self ):

        scout_tile = None

        tiles = self.map_proxy.get_player_visible_tiles()
        border_tiles = self.map_proxy.get_border_tiles()
        
        for position in tiles:
            if position not in border_tiles:
                for base_position in self.map_proxy.get_bases_positions():
                    if not self.map_proxy.is_position_occupied( position ) and self.map_proxy.is_position_on_map( position ):
                        if position.distance_from( base_position ) <= 4:
                            for neighbour in position.get_all_neighbours():
                                if self.map_proxy.is_position_occupied( neighbour ):
                                    scout_tile = position
                                    break
            if scout_tile is not None:
                break
        
        if scout_tile is not None:
            canAttackBase = FilterFactory().attack_filter( CanAttackBaseAttackFilter )

            self.game_control_proxy.spawn_unit(
                SpawnInformation(self.player,
                                    GameObjectType.ARCHER,
                                scout_tile,
                                [ canAttackBase ], []))


    def build_knight(self):
        
        knight_tile = None

        tiles = self.map_proxy.get_player_visible_tiles()
        border_tiles = self.map_proxy.get_border_tiles()
        
        for position in tiles:
            if position not in border_tiles:
                for base_position in self.map_proxy.get_bases_positions():
                    if position.distance_from( base_position ) <= 2:
                        if not self.map_proxy.is_position_occupied( position ) and self.map_proxy.is_position_on_map( position ):
                            for neighbour in position.get_all_neighbours():
                                if self.map_proxy.is_position_occupied( neighbour ):
                                    knight_tile = position
                                    break
            if knight_tile is not None:
                break


        if knight_tile is not None:
            can_attack_base_filter = FilterFactory().attack_filter( CanAttackBaseAttackFilter )
            strongest_filter = FilterFactory().attack_filter(AttackStrongestFilter)

            self.game_control_proxy.spawn_unit(
                SpawnInformation(self.player,
                                GameObjectType.KNIGHT,
                                knight_tile,
                                [can_attack_base_filter, strongest_filter], []))

    def build_druid(self):
        druid_tile = None

        tiles = self.map_proxy.get_player_visible_tiles()
        border_tiles = self.map_proxy.get_border_tiles()
        
        for position in tiles:
            if position not in border_tiles:
                for base_position in self.map_proxy.get_bases_positions():
                    if not self.map_proxy.is_position_occupied( position ) and self.map_proxy.is_position_on_map( position ):
                        if position.distance_from( base_position ) <= 4:
                            for neighbour in position.get_all_neighbours():
                                if self.map_proxy.is_position_occupied( neighbour ):
                                    druid_tile = position
                                    break
            if druid_tile is not None:
                break

        if druid_tile is not None:
            can_attack_base_filter = FilterFactory().attack_filter( CanAttackBaseAttackFilter )
            strongest_filter = FilterFactory().attack_filter(AttackStrongestFilter)
            attack_vulnerable = FilterFactory().attack_filter( AttackMostVulnerableFilter )


            self.game_control_proxy.spawn_unit(
                SpawnInformation(self.player,
                                GameObjectType.KNIGHT,
                                druid_tile,
                                [attack_vulnerable, can_attack_base_filter, strongest_filter], []))

    def build_ent(self):
        ent_tile = None

        tiles = self.map_proxy.get_player_visible_tiles()
        border_tiles = self.map_proxy.get_border_tiles()
        
        for position in tiles:
            if position not in border_tiles:
                for base_position in self.map_proxy.get_bases_positions():
                    if not self.map_proxy.is_position_occupied( position ) and self.map_proxy.is_position_on_map( position ):
                        if position.distance_from( base_position ) <= 2:
                            for neighbour in position.get_all_neighbours():
                                if self.map_proxy.is_position_occupied( neighbour ):
                                    ent_tile = position
                                    break
            if ent_tile is not None:
                break

        if ent_tile is not None:
            can_attack_base_filter = FilterFactory().attack_filter( CanAttackBaseAttackFilter )
            strongest_filter = FilterFactory().attack_filter(AttackStrongestFilter)
            attack_vulnerable = FilterFactory().attack_filter( AttackMostVulnerableFilter )


            self.game_control_proxy.spawn_unit(
                SpawnInformation(self.player,
                                GameObjectType.KNIGHT,
                                ent_tile,
                                [attack_vulnerable, can_attack_base_filter, strongest_filter], []))
