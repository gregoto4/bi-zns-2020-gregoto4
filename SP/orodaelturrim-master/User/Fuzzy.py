from abc import ABC
from typing import List, Dict, Callable, Tuple

class FuzzySet:
  
  _name = ""
  _definition = ""
  _last_dom = 0.0

  def __init__( self, name: str, definition: Callable[[float], float] ):
    self._name = name
    self._definition = definition
    self._last_dom = 0.0

  def name( self ):
    return self._name

  def calculateDOM( self, value: float ):
    self._last_dom = self._definition( value )

    # normalize dom
    if self._last_dom > 1:
      self._last_dom = 1
    elif self._last_dom < 0:
      self._last_dom = 0

  def lastDOM( self ):
    return self._last_dom


class FuzzyVariable():

  _name = ""
  _sets = {}

  def __init__( self, name: str ):
    self._name = name
    self._sets = {}

  def addSet( self, name: str, definition: Callable[[float], float] ):
    self._sets[name] = FuzzySet( name, definition )

  def getDOM( self, name ):
    
    return self._sets[name].lastDOM()

  def name( self ):
    return self._name


class FuzzyInputVariable(FuzzyVariable):

  _sets = {}

  def __init__( self, name: str ):
    super().__init__( str )
  
  def fuzzify( self, value: float ):
    for key in self._sets:
      self._sets[key].calculateDOM( value )

class FuzzyOutputVariable(FuzzyVariable):

  _sets = {}
  
  def __init__( self, name: str ):
    super().__init__( str )

  def defuzzify( self, value: float ):
    maximum = ( "", 0.0 )
    for key in self._sets:
      self._sets[key].calculateDOM( value )
      if ( self._sets[key].lastDOM() > maximum[1] ):
        maximum = ( self._sets[key].name(), self._sets[key].lastDOM() )

    return maximum[0]