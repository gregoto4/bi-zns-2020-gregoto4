from abc import ABC, abstractmethod
from typing import List, Dict

class IFuzzyValue(ABC):
  
  @abstractmethod
  def getValues(self) -> Dict[str, float]:
    pass


class FuzzyBool(IFuzzyValue):
  _value = 0.0

  def __init__( self, expresion: bool ):
    _value = 0.0
    if expresion:
      _value = 1.0

  def getValue( self ):
    return self._value

  def getValues( self ):

    result = {}
    result["value"] = self._value

    return result


class FuzzyDistance(IFuzzyValue):
   
  _close = 0.0
  _far = 0.0
  

  def __init__( self, distance: int ):
    if distance <= 3:
      self._close = 1.0
      self._far = 0.0
    elif distance >= 6:
      self._close = 0.0
      self._far = 1.0
    else:
      self._close = 2 - ( distance / 3.0 )
      self._far = ( distance / 3.0 ) - 1

  def getClose(self):
    return self._close

  def getFar(self):
    return self._far

  def getValues(self):

    result = {}
    result["close"] = self._close
    result["far"] = self._far

    return result

class FuzzyMoney(IFuzzyValue):
  pass