from typing import List

from ExpertSystem.Business.UserFramework import IInference, ActionBaseCaller
from ExpertSystem.Structure.Enums import LogicalOperator
from ExpertSystem.Structure.RuleBase import Rule, Fact, ExpressionNode, Expression
from OrodaelTurrim.Business.Logger import Logger
from User.Fuzzy import FuzzyOutputVariable
from User.ActionBase import ActionBase


class Inference(IInference):
    """
    | User definition of the inference. You can define here you inference method (forward or backward).
      You can have here as many functions as you want, but you must implement interfere with same signature

    |
    | `def interfere(self, knowledge_base: List[Fact], rules: List[Rule], action_base: ActionBase):`
    |

    | Method `interfere` will be called each turn or manually with `Inference` button.
    | Class have no class parameters, you can use only inference parameters

    """
    knowledge_base: List[Fact]
    action_base: ActionBaseCaller


    def infere(self, knowledge_base: List[Fact], rules: List[Rule], action_base: ActionBaseCaller) -> None:
        """
        User defined inference

        :param knowledge_base: - list of Fact classes defined in  KnowledgeBase.create_knowledge_base()
        :param rules:  - list of rules trees defined in rules file.
        :param action_base: - instance of ActionBaseCaller for executing conclusions

        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        !!    TODO: Write implementation of your inference mechanism definition HERE    !!
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        """
        self.knowledge_base = knowledge_base
        self.action_base = action_base

        for rule in rules:
            value = self.rule_evaluation(rule.condition)

            if value > 0.2:
                self.conclusion_evaluation(rule.conclusion, value)


    def rule_evaluation(self, root_node: ExpressionNode) -> float:
        """
        Example of rule tree evaluation. This implementation did not check comparision operators and uncertainty.
        For usage in inference extend this function

        :param root_node: root node of the rule tree
        :return: True if rules is satisfiable, False in case of not satisfiable or missing Facts
        """
        if root_node.operator == LogicalOperator.AND:
            return min( self.rule_evaluation(root_node.left), self.rule_evaluation(root_node.right) )

        elif root_node.operator == LogicalOperator.OR:
            return max( self.rule_evaluation(root_node.left), self.rule_evaluation(root_node.right) )

        elif isinstance(root_node.value, Expression):
            try:
                return self.knowledge_base[self.knowledge_base.index(root_node.value.name)](*root_node.value.args)
            except ValueError:
                return 0.0

        else:
            return float(root_node.value)


    def fuzzy_build_base( self ) -> FuzzyOutputVariable:
        priority = FuzzyOutputVariable( "Build Base" )
        priority.addSet( "build_base", lambda x: 1.0 )

        return priority


    def fuzzy_build_low( self ) -> FuzzyOutputVariable:
        priority = FuzzyOutputVariable( "Low Econ Build" )

        priority.addSet( "build_archer", lambda x: ( 0.1 * ( x - 60 ) ) )
        priority.addSet( "build_knight", lambda x: ( -0.2 * abs( x - 56 ) ) + 0.8 )
        priority.addSet( "build_druid", lambda x: 0.0 )
        priority.addSet( "build_ent", lambda x: 0.0 )
        priority.addSet( "nothing", lambda x: ( -0.15 * ( x - 55 ) ) )

        return priority

    def fuzzy_build_medium( self ) -> FuzzyOutputVariable:
        priority = FuzzyOutputVariable( "Medium Econ Build" )

        priority.addSet( "build_archer", lambda x: ( -0.18 * abs( x - 34 ) ) + 1 )
        priority.addSet( "build_knight", lambda x: ( 0.05 * ( x - 30 ) ) )
        priority.addSet( "build_druid", lambda x: ( -0.3 * abs( x - 48 ) ) + 1.5 )
        priority.addSet( "build_ent", lambda x: 0.0 )
        priority.addSet( "nothing", lambda x: ( -0.15 * ( x - 35 ) ) )

        return priority

    def fuzzy_build_high( self ) -> FuzzyOutputVariable:
        priority = FuzzyOutputVariable( "High Econ Build" )

        priority.addSet( "build_archer", lambda x: 0.0 )
        priority.addSet( "build_knight", lambda x: ( -0.15 * abs( x - 22 ) ) + 2  )
        priority.addSet( "build_druid", lambda x: ( -0.15 * abs( x - 40 ) ) + 3 )
        priority.addSet( "build_ent", lambda x: ( 0.15 * ( x - 50 ) )  )
        priority.addSet( "nothing", lambda x: ( -0.15 * ( x - 25 ) ) )

        return priority

    def fuzzy_scout( self ) -> FuzzyOutputVariable:
        priority = FuzzyOutputVariable( "Build Scouts" )

        priority.addSet( "build_scout", lambda x: 1 )
        priority.addSet( "nothing", lambda x: ( -0.5 * ( x - 50 ) ) + 0.5 )

        return priority
    


    def conclusion_evaluation(self, root_node: ExpressionNode, concusion_dom: float):

        node = root_node

        while node.value is None:
            node = node.left

        build_order = []
        nodes = []

        nodes.append( root_node )

        expresions = []

        while len( nodes ) > 0:

            if nodes[0].operator == LogicalOperator.AND:
                nodes.append( nodes[0].left )
                nodes.append( nodes[0].right )
            elif isinstance(nodes[0].value, Expression):
                expresions.append( nodes[0].value )

            nodes.pop( 0 )

        conclusion = FuzzyOutputVariable( "" )

        if node.value.name == "build_base":
            conclusion = self.fuzzy_build_base()
            Logger.log('Building base')


        elif node.value.name == "build_low":
            conclusion = self.fuzzy_build_low()
            Logger.log('Low econ')


        elif node.value.name == "build_medium":
            conclusion = self.fuzzy_build_medium()
            Logger.log('Medium econ')


        elif node.value.name == "build_high":
            conclusion = self.fuzzy_build_high()
            Logger.log('High econ')

        elif node.value.name == "build_scout":
            conclusion = self.fuzzy_scout()
            Logger.log('Building scout')
        else:
            return None

        conc = conclusion.defuzzify( concusion_dom * 100 )

        if len( expresions ) == 1:
            build_order = expresions[0]
        else:
            for ex in expresions:
                if ex.name == conc:
                    build_order = ex
                    break

        if conc != "nothing" and isinstance(build_order, Expression):
            if self.action_base.has_method( build_order ):
                self.action_base.call( build_order )
            
